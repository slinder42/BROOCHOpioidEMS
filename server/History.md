0.2.0 2017-12-08
==================

  * Added partial includes to Index and Projects views

0.1.20 2017-12-07
==================

  * Added forEach loop to fill out BROOCH team table
  * Added isInvalidPort function to check if manually chosen port can be used for server
  * Added BROOCH Team Table content to include in Contact view
  * Added Page Head template to include on all pages
  * Fixed Contact route and Team json file to properly include BROOCH Team
  * Fixed BROOCH Team Table content to display values in rows
  * Added Page Footer template to include on all pages
  * Fixed isInvalidPort helper function for logging to console and checking for null values
  * Updated reload function for changed Express API
  * Added Page Header template to include on all pages
  * Installed ejs-lint as dev dependency
  * Added Page Footer, then Page Head, then Page Header to About view for troubleshooting
  * Added Mobile Navigation template
  * Implemented partial include on About and Contact views

0.1.7 2017-12-01
==================

  * Added table to Contact static html page for team members
  * Added static html formatting to ejs files

0.1.5 2017-11-23
==================

  * Navbar and mobile navbar added to static html pages
  * Added data file for BROOCH team

0.1.3 2017-11-14
==================

  * Display items linked to Bootstrap classes instead of CSS file
  * Desktop navbar fitted on one line with brand at right
  * Mobile navbar appears besides page content

0.1.0 2017-11-03
==================

  * Added static html pages
  * Set up embedded social media functionality

0.0.1 2017-11-02
==================

  * Initialized git repository
  * Set up basic structure for BROOCH page