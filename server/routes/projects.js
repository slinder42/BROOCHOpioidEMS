var express = require('express');
var router = express.Router();

router.get('/projects', function(req, res) {

	res.render('projects', {
		pageTitle: "Current Projects",
		pageID: "projects",
	});

});

module.exports = router;