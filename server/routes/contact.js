var express = require('express');
var router = express.Router();

router.get('/contact', function(req, res) {

	var BROOCHTeam = req.app.get('BROOCHTeam');
	var BROOCHTeamMembers = BROOCHTeam.TeamMembers;

	res.render('contact', {
		pageTitle: "Contact Us",
		pageID: "contact",
		TeamMembers: BROOCHTeamMembers
	});

});

module.exports = router;