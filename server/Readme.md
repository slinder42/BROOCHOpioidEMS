BROOCH Website Read Me
===============

## Introduction

This is the repository for the BROOCH web site.

## Features

  * Launching site for people to see BROOCH
  * Displays data on social media, organization mission, and projects

## Description

This will be edited later to include more details

## Planned Features

  * Add all projects to site
  * Rewrite code base to minimize duplication of methods
  * Install Node application on web to display
  * Implement testing framework using Node modules
  * Port to PHP for alternate server functionality

## Quick Installation (macOS)

  * Unzip archive file to get BROOCH folder
  * Move folder to Desktop
  * In the Finder, open the Go menu and select Applications
  * In the Applications folder, open the Utilities folder
  * In the Utilities folder, open the Terminal application
  * Enter the following command in the Terminal window: npm -v
  * If the Terminal does not recognize the command, install Node by going to https://nodejs.org and download the recommended version. After installation is complete entering npm -v in the Terminal should give the Node Package Manager version
  * Enter the following command in the Terminal window: cd Desktop
  * Enter the following command in the Terminal window: cd BROOCH
  * Enter the following command in the Terminal window: npm install
  * Enter the following command in the Terminal window: npm start