var express = require('express');
var reload = require('reload');
var bodyParser = require("body-parser");
var site = express();

var BROOCHTeam = require('./data/BROOCHTeam.json');
// var navBar = require('./data/navBar.json');

var sitePort
sitePort = process.env.PORT;
if (isInvalidPort(sitePort)) {
	console.log("Switching to valid port.");
	sitePort = 3000;
}

site.set('port', sitePort);
site.set('BROOCHTeam', BROOCHTeam);
// site.set('navBar', navBar);
site.set('view engine', 'ejs');
site.set('views', 'server/views');

site.locals.siteTitle = 'BROOCH';

site.use(express.static('server/public'));
site.use(require('./routes/index'));
site.use(require('./routes/about'));
site.use(require('./routes/contact'));
site.use(require('./routes/projects'));

site.use(bodyParser.json());
site.use(bodyParser.urlencoded({extended: false}));

// Load JQuery and Bootstrap through NPM Modules
// server.use('/js', express.static(__dirname+"/node_modules/bootstrap/dist/js"));
// server.use('/js', express.static(__dirname+"/node_modules/jquery/dist/"));
// server.use('/css', express.static(__dirname+"/node_modules/bootstrap/dist/css"));

var server = site.listen(site.get('port'), function() {
	console.log('Listening on port ' + site.get('port'));
});

// Normal start script:
// "start": node server/server.js

// Start script for package.json in dev mode:
// "start": "nodemon -e css,ejs,js,json --watch server --ignore NalPurchases.json"

// Start scripts for package.json using pingy:
// "start": "pingy dev",
// "export": "pingy export"

// Start script for showing mockup to laypeople
// "start": node server/serer.js & open http://localhost:3000

reload(site);

function isInvalidPort(port) {
	if (!port) {
		console.log("The assigned port is not defined or is null");
		return true;
	}
	else if (typeof(port)!= 'number') {
		console.log("The assigned port is not a number.");
		return true;
	}
	else if (port < 1024 || port > 65535) {
		console.log("The assigned port is not ephemeral.")
		return true;
	}
	else {
		return false;
	}
}